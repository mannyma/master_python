from .status import Status
class Result(object):
    @staticmethod
    def success(data={}, status=Status.SUCCESS.status, message=Status.SUCCESS.message):
        ret_json = {
            "status":status,
            "message":message,
            "data":data
        }
        return ret_json

    @staticmethod
    def error(data={}, status=Status.ERROR.status, message=Status.ERROR.message):
        ret_json = {
            "status":status,
            "message":message,
            "data":data
        }
        return ret_json
    