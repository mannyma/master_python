# 继承
# 继承父类的方法

class Animal:
    def __init__(self):
        print('Animal init')

    def eat(self):
        print('Animal eat')

class Leg(object):
    pass

class Dog(Animal):
    # 类属性
    _count = 0
    def __init__(self, name):
        self.name = name
        self.leg = Leg()
        Dog._count += 1

    # 狗咬人
    def bid(self, man):
        print(self.name + '咬' + man.name)
    
    def eat(self):
        super().eat()
        print('Dog eat')

    def __str__(self):
        return self.name + "说：目前总共有" + str(Dog._count) + "只狗"
    
    @classmethod
    def add_one(cls):
        cls._count += 1

class Man(Animal):
    def __init__(self, name):
        self.name = name
        self.leg = Leg()

    # 人养狗
    def feed(self, dog):
        print(self.name + '养' + dog.name)

    def eat(self):
        super().eat()
        print('Man eat')

def do_eat(obj):
    obj.eat()

dog1 = Dog('旺财')
man1 = Man('zhangsan')
man1.feed(dog1)
dog1.bid(man1)
do_eat(dog1)
do_eat(man1)
dog2 = Dog('小白')
dog3 = Dog('雪梨')
print(dog1)
print(dog2)
print(dog3)
Dog.add_one()
print(dog2)


# horse
class Horse(Animal):
    def __init__(self):
        #Animal.__init__(self)
        super(Horse,self).__init__()
        print('Horse init')

# lv
class Lv(Animal):
    def __init__(self):
        #Animal.__init__(self)
        super(Lv, self).__init__()
        print('Lv init')

class Luo(Horse, Lv):
    def __init__(self):
        #Horse.__init__(self)
        #Lv.__init__(self)
        super(Luo, self).__init__()
        print('Luo init')

luo = Luo()

# 练习1
# 白蛇传
class 人(object):
    def __init__(self, name):
        self.name = name

    def catch(self, yao):
        print(self.name + '抓' + yao.name)

    def like(self, bsz):
        print(self.name + '喜欢' + bsz.name)

class 妖(object):
    def __init__(self, name):
        self.name = name

class 蛇妖(妖):
    pass

class 白蛇(蛇妖):
    pass

class 青蛇(蛇妖):
    pass

法海 = 人('法海')
许仙 = 人('许仙')
白素贞 = 白蛇('白素贞')
小青 = 青蛇('小青')
法海.catch(白素贞)
法海.catch(小青)
许仙.like(白素贞)


