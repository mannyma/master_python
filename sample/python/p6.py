# 字符串
str = 'helloworld'
print(str[4])
print(str[-1])
print(str[1:3])
print(str[1:])
print(str[:3])

if str.startswith('hello') :
    print("ok")
else :
    print("error")

# 列表
list = [1,2,3,5,8,13,21,34]
list.append(55)
print(list)
list.extend([55,89])
print(list)
list.insert(2,'new')
print(list)
list.remove(5)
print(list)
value1 = list.pop()
print(list)
print(value1)

# tuple
t = (3,)
print(type(t))

# set
s = set()
print(type(s))
set1 = {1,2,3}
set1.add(4)
print(set1)
set1.update({5,6})
print(set1)
set1.remove(2)
print(set1)
# set1.remove(7)
set1.discard(7)

# dict
dict1 = {"name":"yangbing", "age":80, "height":152.3, "married":True}
print(dict1["height"])
print(dict1.get("height2",150.0))
dict1["height"]=162.3
print(dict1)
dict1.pop("married")
print(dict1)

for key,value in dict1.items() :
    print(key,value)

for key in dict1.keys() :
    print(key, dict1.get(key))

# 习题1
p1 = [3,5,2]
p2 = [-1,0,-3]

# 曼哈顿距离

ma_dist = abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]) + abs(p1[2]-p2[2])
print('曼哈顿距离：%s'%ma_dist)

# 欧氏距离
ou_dist = ((p1[0]-p2[0])**2 + (p1[1]-p2[1])*2 + (p1[2]-p2[2])*2) ** 0.5
print('欧氏距离：%s'%ou_dist)

# 习题2
list1 = []
while True:
    dict1 = {}
    print("输入姓名：")
    name = input()
    print("输入年龄：")
    age = int(input())
    print("输入身高：")
    height = float(input())
    dict1["name"] = name
    dict1["age"] = age
    dict1["height"] = height
    list1.append(dict1)
    print("继续(1继续 0退出)?")
    result = int(input())
    if result==0 :
        break
print(list1)



